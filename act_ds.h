/*
**  OSSP act - Abstract Container Types
**  Copyright (c) 1999-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 1999-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP act, an abstract container type library
**  which can be found at http://www.ossp.org/pkg/lib/act/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  act_ds.h: data structure low-level API (declaration)
*/

#ifndef __ACT_DS_H__
#define __ACT_DS_H__

/* the opaque data structure type */
struct act_ds_st;
typedef struct act_ds_st act_ds_t;

/* the magic cookie which identifies a method structure */
#define ACT_DS_METHOD_TAG 0xBEEF

/* types corresponding to the dispatch functions */
typedef void *(*act_ds_new_t)    (act_ctx_t *);
typedef int   (*act_ds_insert_t) (act_ctx_t *, void *, void *, int, void *, int, int);
typedef int   (*act_ds_lookup_t) (act_ctx_t *, void *, void *, int, void **, int *);
typedef int   (*act_ds_delete_t) (act_ctx_t *, void *, void *, int);
typedef int   (*act_ds_size_t)   (act_ctx_t *, void *, long *, long *);
typedef int   (*act_ds_status_t) (act_ctx_t *, void *, char *, int);
typedef int   (*act_ds_free_t)   (act_ctx_t *, void *);

/* the dispatch structure for the data structure method */
typedef struct act_ds_method_st {
    unsigned int    m_tag;
    act_ds_new_t    m_new;
    act_ds_insert_t m_insert;
    act_ds_lookup_t m_lookup;
    act_ds_delete_t m_delete;
    act_ds_size_t   m_size;
    act_ds_status_t m_status;
    act_ds_free_t   m_free;
} act_ds_method_t;

/* ---------------------------------------------------------------------------------- */

typedef struct {
    void   *ptr;
    size_t *len;
} act_blob_t;

#define ACT_BLOB(ptr,len) \
    { (ptr), (len) }

static act_blob_t act_blob(void *ptr, size_t len)
{
    act_blob_t blob;

    blob.ptr = ptr;
    blob.len = len;
    return blob;
}

/* ---------------------------------------------------------------------------------- */

enum {
    /* access control */
    ACT_IT_ACL_READ,
    ACT_IT_ACL_WRITE,
    /* traversal */
    ACT_IT_TR_STATIC,
    ACT_IT_TR_NATURAL,
    ACT_IT_TR_RANDOM,
    ACT_IT_TR_SORT,
    ACT_IT_TR_REVERSE,
    /* absolute position */
    ACT_IT_POS_IT,
    ACT_IT_POS_KEY,
    ACT_IT_POS_FIRST,
    ACT_IT_POS_LAST,
    ACT_IT_POS_INDEX,
    /* relative positioning */
    ACT_IT_GO_UP,
    ACT_IT_GO_DOWN,
    ACT_IT_GO_LEFT,
    ACT_IT_GO_RIGHT,
    ACT_IT_GO_BACK,
    ACT_IT_GO_FRONT,
    ACT_IT_GO_NEXT,
    ACT_IT_GO_PREV,
};

act_rc_t act_it_create   (act_it_t **it, int acl_and_traverse, ...);
act_rc_t act_it_goto     (act_it_t **it, int position);
act_rc_t act_it_destroy  (act_it_t  *it);

/* ---------------------------------------------------------------------------------- */

enum {
    ACT_CB_RWLOCK_CREATE,
    ACT_CB_RWLOCK_ACQUIRE,
    ACT_CB_RWLOCK_RELEASE,
    ACT_CB_RWLOCK_DESTROY,
    ACT_MAX_WRITER,
    ACT_MAX_READER
    ACT_ST_BYTES_TOTAL,
    ACT_ST_BYTES_MGMT,
    ACT_ST_BYTES_USED,
    ACT_ST_BYTES_FREE,
    ACT_ST_ELEM_TOTAL,
    ACT_ST_ELEM_USED,
    ACT_ST_ELEM_FREE,
};

act_rc_t act_ds_create   (act_ds_t **ds, act_ctx_t *ctx, act_ds_method_t *meth);
act_rc_t act_ds_clone    (act_ds_t  *ds, act_ds_t **clone);
act_rc_t act_ds_import   (act_ds_t  *ds, char *buf, size_t *buf, act_cb_t cb);
act_rc_t act_ds_export   (act_ds_t  *ds, char *buf, size_t *buf, act_cb_t cb);
act_rc_t act_ds_destroy  (act_ds_t  *ds);

act_rc_t act_ds_config   (act_ds_t  *ds, const char *fmt, ...);
act_rc_t act_ds_query    (act_ds_t  *ds, const char *fmt, ...);

act_rc_t act_ds_iterator (act_ds_t  *ds, act_it_t **it, int acl_and_traverse, ...);

act_rc_t act_ds_insert   (act_ds_t  *ds, act_it_t  *it, int pos);
act_rc_t act_ds_delete   (act_ds_t  *ds, act_it_t  *it, int pos);

act_rc_t act_ds_write    (act_ds_t  *ds, act_it_t *it, act_blob_t *blob);
act_rc_t act_ds_read     (act_ds_t  *ds, act_it_t *it, act_blob_t *blob);

/* ---------------------------------------------------------------------------------- */

#endif /* __ACT_DS_H__ */

