/*
**  act_vers.c -- Version Information for OSSP act (syntax: C/C++)
**  [automatically generated and maintained by GNU shtool]
*/

#ifdef _ACT_VERS_C_AS_HEADER_

#ifndef _ACT_VERS_C_
#define _ACT_VERS_C_

#define ACT_INT_VERSION 0x000200

typedef struct {
    const int   v_hex;
    const char *v_short;
    const char *v_long;
    const char *v_tex;
    const char *v_gnu;
    const char *v_web;
    const char *v_sccs;
    const char *v_rcs;
} act_int_version_t;

extern act_int_version_t act_int_version;

#endif /* _ACT_VERS_C_ */

#else /* _ACT_VERS_C_AS_HEADER_ */

#define _ACT_VERS_C_AS_HEADER_
#include "act_vers.c"
#undef  _ACT_VERS_C_AS_HEADER_

act_int_version_t act_int_version = {
    0x000200,
    "0.0.0",
    "0.0.0 (06-Jan-2003)",
    "This is OSSP act, Version 0.0.0 (06-Jan-2003)",
    "OSSP act 0.0.0 (06-Jan-2003)",
    "OSSP act/0.0.0",
    "@(#)OSSP act 0.0.0 (06-Jan-2003)",
    "$Id: act_vers.c,v 1.4 2003/01/06 12:10:58 rse Exp $"
};

#endif /* _ACT_VERS_C_AS_HEADER_ */

