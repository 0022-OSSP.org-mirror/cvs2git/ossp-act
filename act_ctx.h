/*
**  OSSP act - Abstract Container Types
**  Copyright (c) 1999-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 1999-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP act, an abstract container type library
**  which can be found at http://www.ossp.org/pkg/lib/act/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  act_ctx.h: context structure (declaration)
*/

#ifndef _ACT_CTX_H_
#define _ACT_CTX_H_

#include "act.h"

/* context limits */
#define ACT_CTX_ENTRY_MAX 63

/* the special null context id */
#define ACT_CTX_ID_NULL 0

/* high level context id constructor */
#define ACT_CTX_ID(type,base,indx) \
        ACT_CTX_ID_CONS(0,0,act_type_##type,ACT_CTX_BASE_##base+(indx))

/* generate a context id from ingredients */
#define ACT_CTX_ID_CONS(spec,lock,type,indx) \
        (__ACT_CTX_ID_SETSPEC(spec)|\
         __ACT_CTX_ID_SETLOCK(lock)|\
         __ACT_CTX_ID_SETTYPE(type)|\
         __ACT_CTX_ID_SETINDX(indx))

/* split a context id into ingredients */
#define ACT_CTX_ID_SPLIT(spec,lock,type,indx,id) \
        (spec) = __ACT_CTX_ID_GETSPEC(id);\
        (lock) = __ACT_CTX_ID_GETLOCK(id);\
        (type) = __ACT_CTX_ID_GETTYPE(id);\
        (indx) = __ACT_CTX_ID_GETINDX(id);

/* split out the index of a context id only */
#define ACT_CTX_ID_INDEX(id) \
        __ACT_CTX_ID_GETINDX(id)

/* internal macros */
#define __ACT_CTX_ID_SETSPEC(val)  _BM_SET(0,10,10,(val))
#define __ACT_CTX_ID_GETSPEC(id)   _BM_GET((id),10,10)
#define __ACT_CTX_ID_SETLOCK(val)  _BM_SET(0,9,9,(val))
#define __ACT_CTX_ID_GETLOCK(id)   _BM_GET((id),9,9)
#define __ACT_CTX_ID_SETTYPE(val)  _BM_SET(0,8,6,(val))
#define __ACT_CTX_ID_GETTYPE(id)   _BM_GET((id),8,6)
#define __ACT_CTX_ID_SETINDX(val)  _BM_SET(0,5,0,(val))
#define __ACT_CTX_ID_GETINDX(id)   _BM_GET((id),5,0)

/* pre-defined base numbers for context ids of sub-libraries */
#define ACT_CTX_BASE_CTX           0
#define ACT_CTX_BASE_MEM           10
#define ACT_CTX_BASE_COMMON        15
#define ACT_CTX_BASE_BITS          25
#define ACT_CTX_BASE_ARRAY         30
#define ACT_CTX_BASE_LIST          35
#define ACT_CTX_BASE_BUFFER        40
#define ACT_CTX_BASE_TREE          45
#define ACT_CTX_BASE_HASH          50
#define ACT_CTX_BASE_USER          55

/* the context ids of the ctx sub-library itself */
#define ACT_CTX_CTX_LOCKING     ACT_CTX_ID_CONS(1,0,act_type_ptr,ACT_CTX_BASE_CTX+0)
#define ACT_CTX_CTX_BINDING     ACT_CTX_ID_CONS(1,0,act_type_ptr,ACT_CTX_BASE_CTX+1)
#define ACT_CTX_CTX_CALLBACK    ACT_CTX_ID_CONS(1,0,act_type_ptr,ACT_CTX_BASE_CTX+2)

/* the opaque context data structure */
struct act_ctx_st;
typedef struct act_ctx_st act_ctx_t;

/* the global default context */
act_ctx_t *act_ctx_default;

/* the context API */
extern act_ctx_t  *act_ctx_new         (void);
extern act_ctx_t  *act_ctx_dup         (act_ctx_t *, act_ctx_t *);
extern int         act_ctx_set         (act_ctx_t *, unsigned int, ...);
extern int         act_ctx_get         (act_ctx_t *, unsigned int, ...);
extern void       *act_ctx_var         (act_ctx_t *, unsigned int);
extern int         act_ctx_lock        (act_ctx_t *, unsigned int);
extern int         act_ctx_unlock      (act_ctx_t *, unsigned int);
extern int         act_ctx_free        (act_ctx_t *);

#endif /* _ACT_CTX_H_ */
