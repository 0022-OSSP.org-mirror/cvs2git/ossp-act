dnl ##
dnl ##  OSSP act - Abstract Container Types
dnl ##  Copyright (c) 1999-2003 Ralf S. Engelschall <rse@engelschall.com>
dnl ##  Copyright (c) 1999-2003 The OSSP Project <http://www.ossp.org/>
dnl ##
dnl ##  This file is part of OSSP act, a abstract container type library
dnl ##  which can be found at http://www.ossp.org/pkg/lib/act/.
dnl ##
dnl ##  Permission to use, copy, modify, and distribute this software for
dnl ##  any purpose with or without fee is hereby granted, provided that
dnl ##  the above copyright notice and this permission notice appear in all
dnl ##  copies.
dnl ##
dnl ##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
dnl ##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
dnl ##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
dnl ##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
dnl ##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
dnl ##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
dnl ##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
dnl ##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
dnl ##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
dnl ##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
dnl ##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
dnl ##  SUCH DAMAGE.
dnl ##
dnl ##  aclocal.m4: Autoconf macros
dnl ##

dnl ##
dnl ##  Support for Configuration Headers
dnl ##
dnl ##  configure.in:
dnl ##    AC_HEADLINE(<short-name>, <long-name>,
dnl ##                <vers-var>, <vers-file>,
dnl ##                <copyright>)
dnl ##

AC_DEFUN(AC_HEADLINE,[dnl
AC_DIVERT_PUSH(NOTICE)dnl
#   configuration header
if test ".`echo dummy [$]@ | grep help`" = .; then
    #   bootstrapping shtool
    ac_prog=[$]0
changequote(, )dnl
    ac_srcdir=`echo $ac_prog | sed -e 's%/[^/][^/]*$%%' -e 's%\([^/]\)/*$%\1%'`
changequote([, ])dnl
    test ".$ac_srcdir" = ".$ac_prog" && ac_srcdir=.
    ac_shtool="$ac_srcdir/shtool"

    #   find out terminal sequences
    TB=`$ac_shtool echo -n -e %B 2>/dev/null`
    TN=`$ac_shtool echo -n -e %b 2>/dev/null`

    #   find out package version
    $3_STR="`$ac_shtool version -l c -d long $ac_srcdir/$4`"
    AC_SUBST($3_STR)

    #   friendly header ;)
    echo "Configuring ${TB}$1${TN} ($2), Version ${TB}${$3_STR}${TN}"
    echo "$5"

    #   additionally find out hex version
    $3_HEX="`$ac_shtool version -l c -d hex $ac_srcdir/$4`"
    AC_SUBST($3_HEX)
fi
AC_DIVERT_POP()
])dnl
