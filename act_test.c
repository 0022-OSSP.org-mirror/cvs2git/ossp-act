/*
**  OSSP act - Abstract Container Types
**  Copyright (c) 1999-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 1999-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP act, an abstract container type library
**  which can be found at http://www.ossp.org/pkg/lib/act/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  act_test.c: test suite
*/

#include <stdio.h>
#include <string.h>

#include "act_p.h"
#include "act_hash.h"

struct kd {
    char *k;
    char *d;
};

#define act_hash_insert_str(h,k,v,o) \
        act_hash_insert((h),(k),strlen((k)),(v),strlen((v))+1,(o))
#define act_hash_lookup_str(h,k,v) \
        act_hash_lookup((h),(k),strlen((k)),(v),NULL)
#define act_hash_delete_str(h,k) \
        act_hash_delete((h),(k),strlen((k)))

int main(int argc, char *argv[])
{
    act_hash_t *h;
    char buf[1024];
    char buf2[1024];
    long i;
    FILE *fp;
    act_ctx_t *ctx;
    char s[1024];
    char *cp;

    act_init();

    ctx = act_ctx_dup(act_ctx_default, NULL);
    act_ctx_set(ctx, ACT_HASH_FUNC, act_hash_fct(cdt));

    if ((h = act_hash_new(ctx)) == NULL) {
        perror("act_hash_new");
        exit(1);
    }

    act_ctx_free(ctx);

    act_hash_status(h, s, sizeof s);
    fprintf(stderr, "%s\n", s);

    fprintf(stderr, "\n**INSERT**\n\n");

    fp = fopen("../data/words", "r");
    i = 1;
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        buf[strlen(buf)-1] = '\0';
        sprintf(buf2, "%ld", i);
        act_hash_insert_str(h, buf, buf2, FALSE);
        if (   (i < 100     && i % 10     == 0)
            || (i < 1000    && i % 100    == 0)
            || (i < 10000   && i % 1000   == 0)
            || (i < 100000  && i % 10000  == 0)
            || (i < 1000000 && i % 100000 == 0)) {
            act_hash_status(h, s, sizeof s);
            fprintf(stderr, "%s\n", s);
        }
        i++;
    }
    fclose(fp);

    act_hash_status(h, s, sizeof s);
    fprintf(stderr, "%s\n", s);

    fprintf(stderr, "\n**LOOKUP**\n\n");

    fp = fopen("../data/words", "r");
    i = 1;
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        buf[strlen(buf)-1] = '\0';
        sprintf(buf2, "%ld", i);
        if (!act_hash_lookup_str(h, buf, (void **)&cp))
            fprintf(stderr, "failed to lookup `%s'\n", buf);
        else {
            if (strcmp(cp, buf2) != 0)
                fprintf(stderr, "value failed for `%s': %s <-> %s\n", buf, buf2, cp);
        }
        i++;
    }
    fclose(fp);

    act_hash_status(h, s, sizeof s);
    fprintf(stderr, "%s\n", s);

    fprintf(stderr, "\n**DELETE**\n\n");

    fp = fopen("../data/words", "r");
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        buf[strlen(buf)-1] = '\0';
        if (   (i < 100     && i % 10     == 0)
            || (i < 1000    && i % 100    == 0)
            || (i < 10000   && i % 1000   == 0)
            || (i < 100000  && i % 10000  == 0)
            || (i < 1000000 && i % 100000 == 0)) {
            act_hash_status(h, s, sizeof s);
            fprintf(stderr, "%s\n", s);
        }
        if (!act_hash_delete_str(h, buf))
            fprintf(stderr, "failed to delete `%s'\n", buf);
        i--;
    }
    fclose(fp);

    act_hash_status(h, s, sizeof s);
    fprintf(stderr, "%s\n", s);

    if (!act_hash_free(h)) {
        perror("act_hash_free");
        exit(1);
    }
    exit(0);
}

