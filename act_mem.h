/*
**  OSSP act - Abstract Container Types
**  Copyright (c) 1999-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 1999-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP act, an abstract container type library
**  which can be found at http://www.ossp.org/pkg/lib/act/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  act_mem.h: memory handling (declaration)
*/

#ifndef _ACT_MEM_H_
#define _ACT_MEM_H_

extern size_t  act_mem_align(size_t);

extern void *(*act_mem_alloc)(size_t);
extern void *(*act_mem_realloc)(void *, size_t);
extern void  (*act_mem_free)(void *);

extern void *(*act_mem_dup)(void *, size_t);
extern void *(*act_mem_set)(void *, int, size_t);
extern void *(*act_mem_move)(void *, const void *, size_t);
extern void *(*act_mem_char)(const void *, unsigned char, size_t);
extern void *(*act_mem_mem)(const void *, size_t, const void *, size_t);
extern int   (*act_mem_cmp)(const void *, const void *, size_t);

extern void   *act_mem_alloc_ctx(act_ctx_t *, size_t);
extern void   *act_mem_realloc_ctx(act_ctx_t *, void *, size_t);
extern void    act_mem_free_ctx(act_ctx_t *, void *);

typedef void *(*act_mem_alloc_t)(size_t);
typedef void *(*act_mem_realloc_t)(void *, size_t);
typedef void  (*act_mem_free_t)(void *);
typedef void *(*act_mem_alloc_ctx_t)(void *, size_t);
typedef void *(*act_mem_realloc_ctx_t)(void *, void *, size_t);
typedef void  (*act_mem_free_ctx_t)(void *, void *);

#define ACT_CTX_MEM_ALLOC   ACT_CTX_ID(ptr,MEM,0)
#define ACT_CTX_MEM_REALLOC ACT_CTX_ID(ptr,MEM,1)
#define ACT_CTX_MEM_FREE    ACT_CTX_ID(ptr,MEM,2)
#define ACT_CTX_MEM_CTXARG  ACT_CTX_ID(ptr,MEM,3)
#define ACT_CTX_MEM_CTXUSE  ACT_CTX_ID(int,MEM,4)

#endif /* _ACT_MEM_H_ */

