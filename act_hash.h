/*
**  OSSP act - Abstract Container Types
**  Copyright (c) 1999-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 1999-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP act, an abstract container type library
**  which can be found at http://www.ossp.org/pkg/lib/act/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  act_hash.h: dynamic hash table API (declaration)
*/

#ifndef _ACT_HASH_H_
#define _ACT_HASH_H_

/* the opaque hashing data structure */
struct act_hash_st;
typedef struct act_hash_st act_hash_t;

/* context entries for hashing */
#define ACT_HASH_METHOD         ACT_CTX_ID(ptr,HASH,0)
#define ACT_HASH_FUNC           ACT_CTX_ID(ptr,HASH,1)
#define ACT_HASH_TABLESIZE      ACT_CTX_ID(int,HASH,2)
#define ACT_HASH_MAXLOADFCTR    ACT_CTX_ID(int,HASH,3)
#define ACT_HASH_MINLOADFCTR    ACT_CTX_ID(int,HASH,4)

/* types corresponding to the dispatch functions */
typedef void *(*act_hash_new_t)    (act_ctx_t *);
typedef int   (*act_hash_insert_t) (act_ctx_t *, void *, void *, int, void *, int, int);
typedef int   (*act_hash_lookup_t) (act_ctx_t *, void *, void *, int, void **, int *);
typedef int   (*act_hash_delete_t) (act_ctx_t *, void *, void *, int);
typedef int   (*act_hash_size_t)   (act_ctx_t *, void *, long *, long *);
typedef int   (*act_hash_status_t) (act_ctx_t *, void *, char *, int);
typedef int   (*act_hash_free_t)   (act_ctx_t *, void *);

/* the dispatch structure for the hash implementation method */
typedef struct act_hash_method_st {
    unsigned int      m_tag;
    act_hash_new_t    m_new;
    act_hash_insert_t m_insert;
    act_hash_lookup_t m_lookup;
    act_hash_delete_t m_delete;
    act_hash_size_t   m_size;
    act_hash_status_t m_status;
    act_hash_free_t   m_free;
} act_hash_method_t;

/* the magic cookie which identifies a method structure */
#define ACT_HASH_METHOD_TAG 0xBEEF

/* the hashing API functions */
act_hash_t *act_hash_new(act_ctx_t *);
act_ctx_t  *act_hash_ctx(act_hash_t *);
int         act_hash_insert(act_hash_t *, void *, int, void *, int, int);
int         act_hash_lookup(act_hash_t *, void *, int, void **, int *);
int         act_hash_delete(act_hash_t *, void *, int);
int         act_hash_size(act_hash_t *, long *, long *);
int         act_hash_status(act_hash_t *, char *, int);
int         act_hash_free(act_hash_t *);

#define act_hash_mth(name) \
        &(act_hash_##name)

#define __act_hash_mth_proto(name) \
        extern act_hash_method_t act_hash_##name

__act_hash_mth_proto(oh);
__act_hash_mth_proto(lh);

typedef unsigned long (*act_hash_fct_t)(unsigned char *, unsigned int);

#define act_hash_fct(name) \
        __act_hash_fct_##name

#define __act_hash_fct_proto(name) \
        extern unsigned long __act_hash_fct_##name(unsigned char *, unsigned int)

__act_hash_fct_proto(djbx33a);
__act_hash_fct_proto(djbx33x);
__act_hash_fct_proto(jedi);
__act_hash_fct_proto(vocong);
__act_hash_fct_proto(cdt);
__act_hash_fct_proto(jotcl);
__act_hash_fct_proto(bjddj);
__act_hash_fct_proto(crc32);
__act_hash_fct_proto(teadm);
__act_hash_fct_proto(cpoaat);
__act_hash_fct_proto(ozsdbm);
__act_hash_fct_proto(fonovo);
__act_hash_fct_proto(kazlib);
__act_hash_fct_proto(buzhash);
__act_hash_fct_proto(pearson);
__act_hash_fct_proto(rifkin);
__act_hash_fct_proto(asu);
__act_hash_fct_proto(holub);
__act_hash_fct_proto(cbu);
__act_hash_fct_proto(cvs);

#endif /* _ACT_HASH_H_ */

