/*
**  OSSP act - Abstract Container Types
**  Copyright (c) 1999-2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 1999-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP act, an abstract container type library
**  which can be found at http://www.ossp.org/pkg/lib/act/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  act_grid.h: memory grid (declaration)
*/

#ifndef __ACT_GRID_H__
#define __ACT_GRID_H__

struct act_grid_st;
typedef struct act_grid_st act_grid_t;

extern act_rc_t act_grid_create (act_grid_t **grid, act_ctx_t *ctx, size_t tile_size, int tile_num);
extern act_rc_t act_grid_alloc  (act_grid_t  *grid, void **tile);
extern act_rc_t act_grid_inside (act_grid_t  *grid, void  *tile);
extern act_rc_t act_grid_free   (act_grid_t  *grid, void  *tile);
extern act_rc_t act_grid_stat   (act_grid_t  *grid, int *chunks, int *bytes_mgmt, int *bytes_used, int *bytes_free, int *tiles_used, int *tiles_free);
extern act_rc_t act_grid_destroy(act_grid_t  *grid);

#endif /* __ACT_GRID_H__ */

